# stats

Tools for collecting and reporting statistical information.

## Usage

Add the following dependency to your Maven POM. 

```xml
<dependency>
  <groupId>edu.byu.hbll</groupId>
  <artifactId>stats</artifactId>
  <version>1.1.0</version>
</dependency>
```

## Times

A collection of tools that gathers benchmarking, occurrence, and timing statistics for processes and subcomponents of processes. The Times class is your starting point. There are different types of statistics that Times supports: Benchmark, History, PerfectHistory. After constructing a new Times with a particular statistical type, start adding occurrences of processes with their accompanying durations by calling mark() or using a Times.Marker object. Break down process stats into smaller components by specifying a label for different spots (ie, mark(Object) or Times.Marker.mark(Object)). Times.Marker should be used if duration statistics are required.

Unless otherwise noted as with `Marker`, the classes in edu.byu.hbll.stats.time are thread safe. All `Statistic` classes are designed to be added to over and over again and by multiple threads. Though `Statistic`s are generally used within the context of the `Times` class, they may also be used standalone.

Example 1:

```java
Marker m = Times.single();
// first component of the process
m.mark(1);
// second component of the process
m.mark(2);
// third component of the process
m.mark(3);
System.out.println(m);
```

Example 2:

```java
Times<Benchmark> t = Times.benchmark();
Marker m = t.marker();
// first component of the process
m.mark(1);
// second component of the process
m.mark(2);
// third component of the process
m.mark(3);
System.out.println(t);
```
 
Example 3:

```java
private Times<History> t = new Times<>(new History(ChronoUnit.MINUTES, 120));
 
public void run() {
    Marker m = t.marker();
    // first component of the process
    m.mark(1);
    // second component of the process
    m.mark(2);
    // third component of the process
    m.mark(3);
}
```

Example 4:

```java
private static Times<PerfectHistory> t = Times.perfectHistory();
 
public void run() {
    // run some process
    t.mark();
}
```

## Developing This Project

Note: These instructions assume Docker and VS Code, with the Dev Containers extension, are installed.

Git clone the project locally and open it with VS Code. When prompted, reopen the project in a dev container. It will take a couple of minutes to download all of the dependencies and prepare the container for Java development. Proceed with development once the initialization is complete.

Tests can be run from within VS Code. Click the Testing icon and click "Run Tests".

To build and install the project locally in order to include it in another project for testing, run the following from the project directory on the host machine.

```
docker run -it -u $UID:$(id -g) -e MAVEN_CONFIG=/var/maven/.m2 -v ~/.m2:/var/maven/.m2 -v .:/project maven mvn -f /project/pom.xml -Duser.home=/var/maven clean package install
```

Building and deploying this project is handled by the GitLab pipeline. There are two possible destinations for the built project: the BYU HBLL internal Maven repository or the central Maven repository. Use the internal repository for non-public releases and the central repository for public open source releases. To instruct the pipeline to deploy to Maven Central, set the `MAVEN_CENTRAL_DEPLOY` CI/CD variable to `true`. Deploying to either the internal or central repository requires manually playing the deploy job. Note: Deployments to Maven Central are permanent.

## License

[License](LICENSE.md)