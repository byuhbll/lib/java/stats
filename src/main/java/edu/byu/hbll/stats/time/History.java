package edu.byu.hbll.stats.time;

import edu.byu.hbll.stats.time.History.Data;
import edu.byu.hbll.stats.util.FormatUtils;
import java.io.Serializable;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import lombok.Getter;

/**
 * Maintains a limited history of timing statistics grouped by the configured timing resolution. The
 * information kept includes number of occurrences and the average duration.
 *
 * @author Charles Draper
 */
public class History implements Statistic<List<Data>> {

  /** Default resolution. */
  public static final ChronoUnit DEFAULT_RESOLUTION = ChronoUnit.HOURS;

  /** Default max history. */
  public static final int DEFAULT_MAX = 24;

  /** Group stats by this resolution. */
  @Getter private final TemporalUnit resolution;

  /** History no larger than this. */
  @Getter private final int max;

  /** The starting nano time (System.nanoTime()) for which all figures are based. */
  private final long nanoStart;

  /** The starting local time for which all figures are based. */
  private final ZonedDateTime startTime;

  /** The history data. */
  private final LinkedList<RawData> data = new LinkedList<>();

  /** The current group. */
  private RawData currentData;

  /** Nanotime of next rollover. */
  private long nextNano;

  /** Constructs a new {@link History} object with defaults for resolution and max. */
  public History() {
    this(DEFAULT_RESOLUTION, DEFAULT_MAX);
  }

  /**
   * Constructs a new {@link History} object with the given resolution and max.
   *
   * @param resolution group statistical data by this unit of time
   * @param max maximum number of groups to keep
   */
  public History(TemporalUnit resolution, int max) {
    this(resolution, max, ZoneId.systemDefault());
  }

  /**
   * Constructs a new {@link History} object with the given resolution, max, and time zone.
   *
   * @param resolution group statistical data by this unit of time
   * @param max maximum number of groups to keep
   * @param zoneId use the specified time-zone
   */
  public History(TemporalUnit resolution, int max, ZoneId zoneId) {
    this(resolution, max, zoneId, System.nanoTime());
  }

  /**
   * Constructs a new {@link History} object with the given resolution, max, and time zone.
   *
   * @param resolution group statistical data by this unit of time
   * @param max maximum number of groups to keep
   * @param zoneId use the specified time-zone
   * @param nanoStart the starting nano time (System.nanoTime()) for which all figures are based.
   */
  protected History(TemporalUnit resolution, int max, ZoneId zoneId, long nanoStart) {
    this(
        resolution,
        max,
        nanoStart,
        ZonedDateTime.now(zoneId).plusNanos(System.nanoTime() - nanoStart));
  }

  /**
   * Constructs a new {@link History} object with the given resolution and max.
   *
   * @param resolution group statistical data by this unit of time
   * @param max maximum number of groups to keep
   * @param nanoStart the starting nano time (System.nanoTime()) for which all figures are based.
   * @param startTime the starting local time for which all figures are based.
   */
  protected History(TemporalUnit resolution, int max, long nanoStart, ZonedDateTime startTime) {
    this.resolution = resolution;
    this.max = max;
    this.nanoStart = nanoStart;
    this.startTime = startTime;
  }

  /** {@inheritDoc} */
  @Override
  public void add(long duration) {
    synchronized (this) {
      // nano time is used because it is ~20x faster than java.time.*
      long time = System.nanoTime();

      // if it's time to rollover to the next time group
      if (time > nextNano) {
        // get the current time
        ZonedDateTime now = startTime.plusNanos(time - nanoStart);

        // determine the range of time for this time group
        ZonedDateTime lower = now.truncatedTo(resolution);
        ZonedDateTime upper = lower.plus(1, resolution);

        // determine when the next rollover will occur
        nextNano = ChronoUnit.NANOS.between(now, upper) + time;

        // catch up if some timing groups were missed
        catchUp(lower);

        // set the current data
        currentData = new RawData(lower);
        currentData.time = lower;

        // add the data to the history
        data.add(currentData);

        // remove old data if history too long
        if (data.size() > max) {
          data.remove(0);
        }
      }

      // calculate average duration
      currentData.duration =
          (currentData.duration * currentData.count + duration) / (currentData.count + 1);

      // increment number of occurrences
      currentData.count++;
    }
  }

  /**
   * Catches up to the given time if groups were missed.
   *
   * @param lower catch up to this point
   */
  private void catchUp(ZonedDateTime lower) {
    if (currentData != null) {
      ZonedDateTime tmp = currentData.time.plus(1, resolution);

      while (tmp.compareTo(lower) < 0) {
        data.add(new RawData(tmp));

        if (data.size() > max) {
          data.remove(0);
        }

        tmp = tmp.plus(1, resolution);
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public History copy() {
    return new History(resolution, max, nanoStart, startTime);
  }

  /** {@inheritDoc} */
  @Override
  public List<Data> snapshot() {
    List<Data> copy = new ArrayList<>();
    catchUp(ZonedDateTime.now().truncatedTo(resolution));

    synchronized (this) {
      for (RawData d : data) {
        Data data = new Data();
        data.setCount(d.count);
        data.setDuration(Duration.ofNanos((long) d.duration));
        data.setTime(d.time);
        copy.add(data);
      }
    }

    return copy;
  }

  /**
   * Returns a human readable view of the stored statistics.
   *
   * @return a human readable view of the stored statistics
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    List<Data> data = snapshot();
    ListIterator<Data> it = data.listIterator(data.size());

    // Iterate in reverse.
    while (it.hasPrevious()) {
      Data datum = it.previous();
      builder.append(datum);
      builder.append("\n");
    }

    return builder.toString().trim();
  }

  /**
   * A single element in the historical data.
   *
   * @author Charles Draper
   */
  private class RawData {

    /** The span of time this group represents. */
    private ZonedDateTime time;

    /** Number of occurrences. */
    private long count;

    /** Average duration within span. */
    private double duration;

    /**
     * Constructions a new {@link Data} for the given chunk of time.
     *
     * @param time the chunk of time this group represents
     */
    private RawData(ZonedDateTime time) {
      this.time = time;
    }
  }

  /**
   * A single element in the historical data.
   *
   * @author Charles Draper
   */
  @lombok.Data
  public static class Data implements Serializable {

    private static final long serialVersionUID = 1L;

    /** The span of time this group represents. */
    private ZonedDateTime time;

    /** Number of occurrences within span. */
    private long count;

    /** Average duration within span. */
    private Duration duration;

    /** {@inheritDoc} */
    @Override
    public String toString() {
      return time.toOffsetDateTime()
          + "|"
          + count
          + "|"
          + FormatUtils.formatDuration(duration.toNanos());
    }
  }
}
