package edu.byu.hbll.stats.time;

import edu.byu.hbll.stats.util.FormatUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Class that collects timing statistics for benchmarking and reporting. There are different types
 * of statistics for benchmarking or history. After constructing a new {@link Times} with a
 * particular statistical type, start adding occurrences of processes with their accompanying
 * durations by calling {@link #mark()} or using a {@link Marker}. Break down process stats into
 * smaller components by specifying a label for different spots (ie, {@link #mark(Object)} or {@link
 * Times.Marker#mark(Object)}. The latter is necessary for calculating durations.
 *
 * <p>{@link Times} is thread safe.
 *
 * <p>Example 1:
 *
 * <pre>
 * Marker m = Times.single();
 * &#47;&#47; first component of the process
 * m.mark(1);
 * &#47;&#47; second component of the process
 * m.mark(2);
 * &#47;&#47; third component of the process
 * m.mark(3);
 * System.out.println(m);
 * </pre>
 *
 * <p>Example 2:
 *
 * <pre>
 * Times&lt;Benchmark&gt; t = Times.benchmark();
 * Marker m = t.marker();
 * &#47;&#47; first component of the process
 * m.mark(1);
 * &#47;&#47; second component of the process
 * m.mark(2);
 * &#47;&#47; third component of the process
 * m.mark(3);
 * System.out.println(t);
 * </pre>
 *
 * <p>Example 3:
 *
 * <pre>
 * private Times&lt;History&gt; t = new Times&lt;&gt;(new History(ChronoUnit.MINUTES, 120));
 *
 * public void run() &#123;
 *     Marker m = t.marker();
 *     &#47;&#47; first component of the process
 *     m.mark(1);
 *     &#47;&#47; second component of the process
 *     m.mark(2);
 *     &#47;&#47; third component of the process
 *     m.mark(3);
 * &#125;
 * </pre>
 *
 * <p>Example 4:
 *
 * <pre>
 * private static Times&lt;PerfectHistory&gt; t = Times.perfectHistory();
 *
 * public void run() &#123;
 *     &#47;&#47; run some process
 *     t.mark();
 * &#125;
 * </pre>
 *
 * @see Benchmark
 * @see History
 * @see PerfectHistory
 * @param <T> the {@link Statistic} implementation
 * @author Charles Draper
 */
public class Times<T extends Statistic<?>> {

  /**
   * List of statistics, one for each unique label when calling {@link Marker#mark(Object label)}.
   */
  private final Map<Object, T> statistics = Collections.synchronizedMap(new LinkedHashMap<>());

  /** Statistic to copy and reset for each new label. */
  private final T config;

  /**
   * Creates a new {@link Times} based on the {@link Benchmark} statistic.
   *
   * @return the newly created {@link Times}
   */
  public static Times<Benchmark> benchmark() {
    return new Times<Benchmark>(new Benchmark());
  }

  /**
   * Creates a new {@link Times} based on the {@link History} statistic.
   *
   * @return the newly created {@link Times}
   */
  public static Times<History> history() {
    return new Times<History>(new History());
  }

  /**
   * Creates a new {@link Times} based on the {@link PerfectHistory} statistic.
   *
   * @return the newly created {@link Times}
   */
  public static Times<PerfectHistory> perfectHistory() {
    return new Times<PerfectHistory>(new PerfectHistory());
  }

  /**
   * Creates a new one time {@link Marker} for simple duration statistics accessible through {@link
   * Marker#toString()}.
   *
   * @return the newly created {@link Marker}
   */
  public static Marker single() {
    return new Times<Benchmark>(new Benchmark(Benchmark.DEFAULT_SMOOTHING_FACTOR, true)).marker();
  }

  /**
   * Creates a new {@link Times} based on the given statistic.
   *
   * @param config the statistic to copy for each new label
   */
  public Times(T config) {
    this.config = config;
  }

  /**
   * Creates a marker for marking start and end points for duration calculations. Only needed if
   * interested in durations.
   *
   * @return the marker
   */
  public Marker marker() {
    Marker marker = new Marker(this);
    marker.init();
    return marker;
  }

  /** Same as {@link #mark(Object)} with a label of 1. */
  public void mark() {
    mark(1);
  }

  /**
   * Marks an occurrence of a process to be added to the statistics. Use this if durations are not
   * needed.
   *
   * @param label the label to identify the statistic
   */
  public void mark(Object label) {
    getOrCreate(label).add(0);
  }

  /**
   * Gets the list of statistics associated with this label.
   *
   * @param label the label
   * @return the statistic
   */
  @SuppressWarnings("unchecked")
  private T getOrCreate(Object label) {
    return statistics.computeIfAbsent(label, k -> (T) config.copy());
  }

  /**
   * Returns a human readable view of the stored statistics.
   *
   * @return a human readable view of the stored statistics
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    List<Object> labels = new ArrayList<>(getLabels());

    double maxDuration = Double.MIN_VALUE;

    for (T statistic : statistics.values()) {
      if (statistic instanceof Benchmark) {
        maxDuration = Math.max(maxDuration, ((Benchmark) statistic).snapshot().getAverageRecent());
      }
    }

    TimeUnit durationUnit = FormatUtils.unitDuration(maxDuration);

    for (int j = 0; j < labels.size(); j++) {
      Object label = labels.get(j);
      T statistic = getOrCreate(label);

      if (statistic instanceof Benchmark) {
        builder.append(label);
        builder.append("-");
        builder.append(((Benchmark) statistic).toString(durationUnit));
        builder.append(j + 1 == labels.size() ? "\n" : "  ");
      } else {
        builder.append(label);
        builder.append("-");
        builder.append(statistic.toString().replaceAll("\n", "\n" + label + "-"));
        builder.append("\n");
      }
    }

    return builder.toString().trim();
  }

  /**
   * Returns the current list of labels.
   *
   * @return the list of labels
   */
  public List<Object> getLabels() {
    return new ArrayList<>(statistics.keySet());
  }

  /**
   * Gets the first statistic for this {@link Times} or null if one doesn't yet exist.
   *
   * @return the first statistic or null
   */
  public T getStatistic() {
    List<T> statistics = getStatistics();

    if (statistics.isEmpty()) {
      return null;
    } else {
      return statistics.get(0);
    }
  }

  /**
   * Get the statistic for the given label or null if the label is not recognized.
   *
   * @param label the label
   * @return the associated statistic or null
   */
  public T getStatistic(Object label) {
    return statistics.get(label);
  }

  /**
   * Get the current list of statistics.
   *
   * @return the list of statistics
   */
  public List<T> getStatistics() {
    return new ArrayList<>(statistics.values());
  }

  /**
   * Class that marks the start and end times for calculating duration. The method {@link #mark()}
   * or {@link #mark(Object label)} may be called multiple times per marker.
   *
   * <p>IMPORTANT: Unlike {@link Times}, {@link Marker} is NOT thread safe. Each instance is meant
   * to only be used by the thread that created it.
   *
   * @author Charles Draper
   */
  public static class Marker {

    /** The parent {@link Times} instance. */
    private Times<?> times;

    /** Last time mark was called. */
    private long last;

    /** Number of times mark was called. */
    private int count = 1;

    /**
     * Create a new Marker with the given times.
     *
     * @param times the parent {@link Times} object
     */
    private Marker(Times<?> times) {
      this.times = times;
    }

    /**
     * Initializes last. This is done outside of the constructor because finalizing the construction
     * of an object takes time and therefore makes the first mark seem to take longer than it really
     * does.
     */
    private void init() {
      last = System.nanoTime();
    }

    /** Marks the current time with an incrementing counter as the label. */
    public void mark() {
      mark(count++);
    }

    /**
     * Marks the current time with the given label.
     *
     * @param label the label to identify the statistic.
     */
    public void mark(Object label) {
      long now = System.nanoTime();
      long duration = now - last;
      Statistic<?> statistic = times.getOrCreate(label);
      statistic.add(duration);
      last = System.nanoTime();
    }

    /** Same as {@link Times#toString()}. */
    @Override
    public String toString() {
      return times.toString();
    }
  }
}
