/**
 * Statistical tools for benchmarking and reporting based on time and duration.
 *
 * @see Times
 * @author Charles Draper
 */
package edu.byu.hbll.stats.time;
