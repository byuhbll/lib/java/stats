package edu.byu.hbll.stats.time;

/**
 * Hold statistical information for when a particular process runs and its duration. That
 * information is updated by {@link #add(long)} as the data is collected.
 *
 * <p>Implementations of this interface are thread safe.
 *
 * @param <T> the type of data this statistic returns
 * @author Charles Draper
 */
public interface Statistic<T> {

  /**
   * Signals to the statistic that the process has run and how long it took. Calls to this method
   * should be done in real time because implementations will use the current system time to
   * calculate other statistics.
   *
   * @param duration the duration in nanoseconds
   */
  void add(long duration);

  /**
   * Makes a copy of this statistic and resets it. Created specifically for {@link Times} to be able
   * to add new labels.
   *
   * @return a reset copy of this
   */
  Statistic<T> copy();

  /**
   * The statistical data held by this data structure can be in constant flux. This grabs the
   * current data and returns it in a non-updating data structure.
   *
   * @return a snapshot of the current statistical data.
   */
  T snapshot();
}
