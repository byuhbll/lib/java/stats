package edu.byu.hbll.stats.time;

import edu.byu.hbll.stats.time.PerfectHistory.Data;
import edu.byu.hbll.stats.util.FormatUtils;
import java.io.Serializable;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import lombok.Getter;

/**
 * Maintains a limited history of every occurrence of a process keeping both the time it occurred
 * and its duration.
 *
 * @author Charles Draper
 */
public class PerfectHistory implements Statistic<List<Data>> {

  /** Default max number of occurrences to keep. */
  public static final int DEFAULT_MAX = 100;

  /** The list of occurrences. */
  private LinkedList<RawData> data = new LinkedList<>();

  /** Max number of occurrences to keep. */
  @Getter private final int max;

  /** The starting nano time (System.nanoTime()) for which all figures are based. */
  private final long nanoStart;

  /** The starting local time for which all figures are based. */
  private final ZonedDateTime startTime;

  /** Constructs a new {@link PerfectHistory} using the default max history. */
  public PerfectHistory() {
    this(DEFAULT_MAX);
  }

  /**
   * Constructs a new {@link PerfectHistory} using the given max history.
   *
   * @param max maximum number of occurrences to keep
   */
  public PerfectHistory(int max) {
    this(max, ZoneId.systemDefault(), System.nanoTime());
  }

  /**
   * Constructs a new {@link PerfectHistory} using the given max history and zoneId.
   *
   * @param max maximum number of occurrences to keep
   * @param zoneId use the specified time-zone
   */
  public PerfectHistory(int max, ZoneId zoneId) {
    this(max, zoneId, System.nanoTime());
  }

  /**
   * Constructs a new {@link PerfectHistory} using the given max history and zoneId.
   *
   * @param max maximum number of occurrences to keep
   * @param zoneId use the specified time-zone
   * @param nanoStart the starting nano time (System.nanoTime()) for which all figures are based.
   */
  protected PerfectHistory(int max, ZoneId zoneId, long nanoStart) {
    this(max, nanoStart, ZonedDateTime.now(zoneId).plusNanos(System.nanoTime() - nanoStart));
  }

  /**
   * Constructs a new {@link PerfectHistory} using the given max history and zoneId.
   *
   * @param max maximum number of groups to keep
   * @param nanoStart the starting nano time (System.nanoTime()) for which all figures are based.
   * @param startTime the starting local time for which all figures are based.
   */
  protected PerfectHistory(int max, long nanoStart, ZonedDateTime startTime) {
    this.max = max;
    this.nanoStart = nanoStart;
    this.startTime = startTime;
  }

  /** {@inheritDoc} */
  @Override
  public void add(long duration) {
    synchronized (this) {
      // nano time is used because it is ~20x faster than java.time.*
      RawData d = new RawData(System.nanoTime(), duration);
      data.add(d);

      // remove old data if history too long
      if (data.size() > max) {
        data.remove(0);
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public PerfectHistory copy() {
    return new PerfectHistory(max, nanoStart, startTime);
  }

  /** {@inheritDoc} */
  @Override
  public List<Data> snapshot() {
    List<Data> copy = new ArrayList<>();

    synchronized (this) {
      for (RawData d : data) {
        Data data = new Data();
        data.setDuration(Duration.ofNanos(d.duration));
        data.setTime(startTime.plusNanos(d.time - nanoStart));
        copy.add(data);
      }
    }

    return copy;
  }

  /**
   * Returns a human readable view of the stored statistics.
   *
   * @return a human readable view of the stored statistics
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();

    List<Data> data = snapshot();
    ListIterator<Data> it = data.listIterator(data.size());

    // Iterate in reverse.
    while (it.hasPrevious()) {
      Data datum = it.previous();
      builder.append(datum);
      builder.append("\n");
    }

    return builder.toString().trim();
  }

  /**
   * A single element in the historical data.
   *
   * @author Charles Draper
   */
  private class RawData {

    /** Occurrence date and time. */
    private long time;

    /** Duration of the process. */
    private long duration;

    /**
     * Constructions a new {@link Data} for the given duration.
     *
     * @param time when the process ran in nano time
     * @param duration duration of the process
     */
    private RawData(long time, long duration) {
      this.time = time;
      this.duration = duration;
    }
  }

  /**
   * A single element in the historical data.
   *
   * @author Charles Draper
   */
  @lombok.Data
  public static class Data implements Serializable {

    private static final long serialVersionUID = 1L;

    /** Occurrence date and time. */
    private ZonedDateTime time;

    /** Duration of the process. */
    private Duration duration;

    /**
     * Returns a human readable view of the stored statistics.
     *
     * @return a human readable view of the stored statistics
     */
    @Override
    public String toString() {
      return time.toOffsetDateTime() + "|" + FormatUtils.formatDuration(duration.toNanos());
    }
  }
}
