package edu.byu.hbll.stats.time;

import edu.byu.hbll.stats.time.Benchmark.Data;
import edu.byu.hbll.stats.util.FormatUtils;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;
import lombok.Getter;

/**
 * Keeps a weighted average of the duration of processes and their throughput. Throughput is
 * calculated as occurrences per second.
 *
 * @author Charles Draper
 */
public class Benchmark implements Statistic<Data> {

  /** The default smoothing factor for exponential smoothing. */
  public static final double DEFAULT_SMOOTHING_FACTOR = 0.1f;

  /** Number of nanos in a second. */
  private static final long NANOS_PER_SECOND = TimeUnit.SECONDS.toNanos(1);

  /** The smoothing factor for exponential smoothing of recent duration and throughput. */
  @Getter private final double smoothingFactor;

  /** Whether or not to only gather duration statistics. */
  @Getter private final boolean durationsOnly;

  /** The starting nano time (System.nanoTime()) for which all figures are based. */
  private final long nanoStart;

  /** Total number of occurrences. */
  private long count;

  /** The minimum duration in nanoseconds. */
  private long min = Long.MAX_VALUE;

  /** The maximum duration in nanoseconds. */
  private long max = Long.MIN_VALUE;

  /** The average duration in nanoseconds. */
  private double average;

  /** The average duration in nanoseconds smoothed using exponential smoothing. */
  private double averageRecent;

  /** The recent throughput smoothed using exponential smoothing. */
  private double throughputRecent;

  /** Used to increment throughput for the current second. */
  private int throughputCounter;

  /** The current second in nano time. */
  private long currentSecond;

  /** Creates a new {@link Benchmark} with the default smoothing factor. */
  public Benchmark() {
    this(DEFAULT_SMOOTHING_FACTOR);
  }

  /**
   * Creates a new {@link Benchmark} with the given smoothing factor.
   *
   * @param smoothingFactor the smoothing factor for exponential smoothing of recent duration and
   *     throughput
   */
  public Benchmark(double smoothingFactor) {
    this(smoothingFactor, false);
  }

  /**
   * Creates a new {@link Benchmark} with the given smoothing factor and durationsOnly flag.
   *
   * @param smoothingFactor the smoothing factor for exponential smoothing of recent duration and
   *     throughput
   * @param durationsOnly Whether or not to only gather duration statistics.
   */
  public Benchmark(double smoothingFactor, boolean durationsOnly) {
    this(smoothingFactor, durationsOnly, System.nanoTime());
  }

  /**
   * Creates a new {@link Benchmark} with the given smoothing factor.
   *
   * @param smoothingFactor the smoothing factor for exponential smoothing of recent duration and
   *     throughput
   * @param durationsOnly Whether or not to only gather duration statistics.
   * @param nanoStart the starting nano time (System.nanoTime()) for which all figures are based.
   */
  protected Benchmark(double smoothingFactor, boolean durationsOnly, long nanoStart) {
    this.smoothingFactor = smoothingFactor;
    this.durationsOnly = durationsOnly;
    this.nanoStart = nanoStart;
  }

  /** {@inheritDoc} */
  @Override
  public void add(long duration) {
    synchronized (this) {
      count++;
      average = (average * (count - 1) + duration) / count;
      averageRecent =
          averageRecent == 0
              ? average
              : smoothingFactor * duration + (1 - smoothingFactor) * averageRecent;

      if (!durationsOnly) {
        // nano time is used because it is ~20x faster than java.time.*
        long time = System.nanoTime();
        min = Math.min(min, duration);
        max = Math.max(max, duration);
        updateThroughput(time, true);
      }
    }
  }

  /**
   * Updates the throughput each time a second rolls over otherwise just count up.
   *
   * @param time the current nano time
   * @param hit whether this is an actual occurrence or just an update for toString
   */
  private void updateThroughput(long time, boolean hit) {
    synchronized (this) {
      // determine the current second according to nano time
      long second = (time - nanoStart) / NANOS_PER_SECOND;

      // just increment if we're still in the same second as last time
      // if a greater time gets in before a lesser time, the lesser time is considered part of the
      // greater's
      // second
      if (second <= currentSecond) {
        if (hit) {
          throughputCounter++;
        }

        return;
      }

      // update throughput especially including any missed seconds since the last
      for (long i = currentSecond; i < second; i++) {
        throughputRecent =
            throughputRecent == 0
                ? throughputCounter
                : smoothingFactor * throughputCounter + (1 - smoothingFactor) * throughputRecent;
        throughputCounter = 0;
      }

      // set the current second
      currentSecond = second;

      // was this an actual occurrence or just an update for toString
      throughputCounter = hit ? 1 : 0;
    }
  }

  @Override
  public Benchmark copy() {
    return new Benchmark(smoothingFactor, durationsOnly, nanoStart);
  }

  /**
   * Returns a human readable view of the stored statistics.
   *
   * @return a human readable view of the stored statistics
   */
  @Override
  public String toString() {
    return snapshot().toString();
  }

  /**
   * Returns a human readable view of the stored statistics with duration in durationUnit.
   *
   * @param durationUnit format duration in this unit
   * @return a human readable view of the stored statistics with duration in durationUnit
   */
  public String toString(TimeUnit durationUnit) {
    return snapshot().toString(durationUnit);
  }

  /** {@inheritDoc} */
  @Override
  public Data snapshot() {
    updateThroughput(System.nanoTime(), false);

    Data data = new Data();
    data.durationsOnly = durationsOnly;

    synchronized (this) {
      data.average = average;
      data.averageRecent = averageRecent;
      data.count = count;
      data.max = max;
      data.min = min;
      data.throughputRecent = throughputRecent;
    }

    data.throughput =
        ((double) data.count) / Math.max(1, System.nanoTime() - nanoStart) * NANOS_PER_SECOND;

    return data;
  }

  /** A snapshot of the statistical data. */
  @lombok.Data
  public static class Data implements Serializable {

    private static final long serialVersionUID = 1L;

    /** Whether or not to only gather duration statistics. */
    private boolean durationsOnly;

    /** Total number of occurrences. */
    private long count;

    /** The minimum duration in nanoseconds. */
    private long min;

    /** The maximum duration in nanoseconds. */
    private long max;

    /** The average duration in nanoseconds. */
    private double average;

    /** The average duration in nanoseconds smoothed using exponential smoothing. */
    private double averageRecent;

    /** The throughput since the beginning. */
    private double throughput;

    /** The recent throughput smoothed using exponential smoothing. */
    private double throughputRecent;

    /**
     * Returns a human readable view of the stored statistics.
     *
     * @return a human readable view of the stored statistics
     */
    @Override
    public String toString() {
      return toString(FormatUtils.unitDuration(averageRecent));
    }

    /**
     * Returns a human readable view of the stored statistics with duration in durationUnit.
     *
     * @param durationUnit format duration in this unit
     * @return a human readable view of the stored statistics with duration in durationUnit
     */
    public String toString(TimeUnit durationUnit) {
      if (durationsOnly) {
        return FormatUtils.formatDuration(
            averageRecent, FormatUtils.DEFAULT_PRECISION, durationUnit);
      } else {
        return count
            + "|"
            + FormatUtils.formatDuration(averageRecent, FormatUtils.DEFAULT_PRECISION, durationUnit)
            + "|"
            + FormatUtils.formatThroughput(throughputRecent);
      }
    }
  }
}
