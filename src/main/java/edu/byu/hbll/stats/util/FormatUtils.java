package edu.byu.hbll.stats.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.concurrent.TimeUnit;

/**
 * String format utilities for duration and throughput.
 *
 * @author Charles Draper
 */
public final class FormatUtils {

  /** Various time units expressed in nanoseconds. */
  private static final long NANOS_PER_MICRO = TimeUnit.MICROSECONDS.toNanos(1);

  private static final long NANOS_PER_MILLI = TimeUnit.MILLISECONDS.toNanos(1);
  private static final long NANOS_PER_SECOND = TimeUnit.SECONDS.toNanos(1);
  private static final long NANOS_PER_MINUTE = TimeUnit.MINUTES.toNanos(1);
  private static final long NANOS_PER_HOUR = TimeUnit.HOURS.toNanos(1);
  private static final long NANOS_PER_DAY = TimeUnit.DAYS.toNanos(1);

  /** Various time units expressed in seconds. */
  private static final long SECONDS_PER_MINUTE = TimeUnit.MINUTES.toSeconds(1);

  private static final long SECONDS_PER_HOUR = TimeUnit.HOURS.toSeconds(1);

  /** The default precision (number of significant figures). */
  public static final int DEFAULT_PRECISION = 3;

  /** This is a utility class. */
  private FormatUtils() {}

  /**
   * Returns an abbreviation for the given time unit.
   *
   * @param unit the time unit
   * @return an abbreviation for the given time unit
   */
  public static String abbreviation(TimeUnit unit) {
    switch (unit) {
      case NANOSECONDS:
        return "ns";
      case MICROSECONDS:
        return "us";
      case MILLISECONDS:
        return "ms";
      case SECONDS:
        return "s";
      case MINUTES:
        return "m";
      case HOURS:
        return "h";
      case DAYS:
        return "d";
      default:
        return unit.toString();
    }
  }

  /**
   * Formats a value according to the given precision.
   *
   * @param value value to format
   * @param precision number of significant figures
   * @return formatted value
   */
  public static String format(double value, int precision) {
    return new BigDecimal(value).round(new MathContext(precision)).toPlainString();
  }

  /**
   * Formats duration into a human readable format.
   *
   * @param duration duration in nanoseconds
   * @return duration in a human readable format
   */
  public static String formatDuration(double duration) {
    return formatDuration(duration, DEFAULT_PRECISION);
  }

  /**
   * Formats duration into a human readable format.
   *
   * @param duration duration in nanoseconds
   * @param precision number of significant figures
   * @return duration in a human readable format
   */
  public static String formatDuration(double duration, int precision) {
    return formatDuration(duration, precision, unitDuration(duration));
  }

  /**
   * Formats duration into a human readable format.
   *
   * @param duration duration in nanoseconds
   * @param precision number of significant figures
   * @param destUnit convert to this time unit
   * @return duration in a human readable format
   */
  public static String formatDuration(double duration, int precision, TimeUnit destUnit) {
    String formatted = format(duration / destUnit.toNanos(1), precision);
    return formatted + abbreviation(destUnit);
  }

  /**
   * Formats throughput into a human readable format.
   *
   * @param throughput throughput per second
   * @return throughput in a human readable format
   */
  public static String formatThroughput(double throughput) {
    return formatThroughput(throughput, DEFAULT_PRECISION);
  }

  /**
   * Formats throughput into a human readable format.
   *
   * @param throughput throughput per second
   * @param precision number of significant figures
   * @return throughput in a human readable format
   */
  public static String formatThroughput(double throughput, int precision) {
    return formatThroughput(throughput, precision, unitThroughput(throughput));
  }

  /**
   * Formats throughput into a human readable format.
   *
   * @param throughput throughput per second
   * @param precision number of significant figures
   * @param destUnit convert to this time unit
   * @return throughput in a human readable format
   */
  public static String formatThroughput(double throughput, int precision, TimeUnit destUnit) {
    String formatted = format(throughput * destUnit.toSeconds(1), precision);
    return formatted + "p" + abbreviation(destUnit);
  }

  /**
   * Determines the best matching time unit for the given duration.
   *
   * @param duration duration in nanoseconds
   * @return the best matching time unit
   */
  public static TimeUnit unitDuration(double duration) {
    if (duration < NANOS_PER_MICRO) {
      return TimeUnit.NANOSECONDS;
    } else if (duration < NANOS_PER_MILLI) {
      return TimeUnit.MICROSECONDS;
    } else if (duration < NANOS_PER_SECOND) {
      return TimeUnit.MILLISECONDS;
    } else if (duration < NANOS_PER_MINUTE) {
      return TimeUnit.SECONDS;
    } else if (duration < NANOS_PER_HOUR) {
      return TimeUnit.MINUTES;
    } else if (duration < NANOS_PER_DAY) {
      return TimeUnit.HOURS;
    } else {
      return TimeUnit.DAYS;
    }
  }

  /**
   * Determines the best matching time unit for the given throughput.
   *
   * @param throughput throughput per second
   * @return the best matching time unit
   */
  public static TimeUnit unitThroughput(double throughput) {
    double flipped = 1.0 / throughput;

    if (flipped <= 1) {
      return TimeUnit.SECONDS;
    } else if (flipped <= SECONDS_PER_MINUTE) {
      return TimeUnit.MINUTES;
    } else if (flipped <= SECONDS_PER_HOUR) {
      return TimeUnit.HOURS;
    } else {
      return TimeUnit.DAYS;
    }
  }
}
