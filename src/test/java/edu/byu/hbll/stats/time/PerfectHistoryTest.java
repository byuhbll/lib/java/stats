package edu.byu.hbll.stats.time;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.byu.hbll.stats.time.PerfectHistory.Data;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PerfectHistoryTest {

  PerfectHistory perfectHistory;
  long nanoStart;
  ZonedDateTime startTime;

  /**
   * @throws java.lang.Exception
   */
  @BeforeEach
  public void setUp() throws Exception {
    nanoStart = System.nanoTime();
    startTime = ZonedDateTime.now();
    perfectHistory = new PerfectHistory(4, nanoStart, startTime);
    perfectHistory.add(0);
    perfectHistory.add(1);
    perfectHistory.add(2);
    perfectHistory.add(3);
    perfectHistory.add(4);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.PerfectHistory#PerfectHistory()}. */
  @Test
  public void testPerfectHistory() {
    perfectHistory = new PerfectHistory();
    assertEquals(PerfectHistory.DEFAULT_MAX, perfectHistory.getMax());
  }

  /** Test method for {@link edu.byu.hbll.stats.time.PerfectHistory#PerfectHistory(int)}. */
  @Test
  public void testPerfectHistoryInt() {
    perfectHistory = new PerfectHistory(17);
    assertEquals(17, perfectHistory.getMax());
  }

  /** Test method for {@link edu.byu.hbll.stats.time.PerfectHistory#PerfectHistory(int, long)}. */
  @Test
  public void testPerfectHistoryIntLong() throws Exception {
    long nanoStart = System.nanoTime();
    perfectHistory = new PerfectHistory(17, ZoneId.systemDefault(), nanoStart);
    assertEquals(17, perfectHistory.getMax());
  }

  /**
   * Test method for {@link edu.byu.hbll.stats.time.PerfectHistory#PerfectHistory(int, long,
   * java.time.LocalDateTime)}.
   */
  @Test
  public void testPerfectHistoryIntLongLocalDateTime() throws Exception {
    long nanoStart = System.nanoTime();
    ZonedDateTime startTime = ZonedDateTime.now();
    perfectHistory = new PerfectHistory(17, nanoStart, startTime);
    assertEquals(17, perfectHistory.getMax());
  }

  /** Test method for {@link edu.byu.hbll.stats.time.PerfectHistory#add(long, long)}. */
  @Test
  public void testAdd() {
    perfectHistory.add(5);
    assertEquals(4, perfectHistory.snapshot().size());
  }

  /** Test method for {@link edu.byu.hbll.stats.time.PerfectHistory#copy()}. */
  @Test
  public void testCopy() {
    PerfectHistory copy = perfectHistory.copy();
    assertEquals(4, copy.getMax());
    assertTrue(copy.snapshot().isEmpty());
  }

  /** Test method for {@link edu.byu.hbll.stats.time.PerfectHistory#snapshot()}. */
  @Test
  public void testSnapshot() throws Exception {
    List<Data> data = perfectHistory.snapshot();

    for (int i = 0; i < 4; i++) {
      assertEquals(Duration.ofNanos(i + 1), data.get(i).getDuration());
      assertEquals(0, ChronoUnit.SECONDS.between(LocalDateTime.now(), data.get(i).getTime()));
    }
  }

  /** Test method for {@link edu.byu.hbll.stats.time.PerfectHistory#getMax()}. */
  @Test
  public void testGetMax() {
    assertEquals(4, perfectHistory.getMax());
  }

  /** Test method for {@link edu.byu.hbll.stats.time.PerfectHistory#toString()}. */
  @Test
  public void testToString() {
    String matchString = ".*4ns\n.*3ns\n.*2ns\n.*1ns.*";
    String perfectHistoryString = perfectHistory.toString();
    assertTrue(perfectHistoryString.matches(matchString));
  }

  /**
   * Test method for {@link edu.byu.hbll.stats.time.PerfectHistory#PerfectHistory(int,
   * java.time.ZoneId)}.
   */
  @Test
  public void testPerfectHistoryIntZoneId() throws Exception {
    PerfectHistory perfectHistory = new PerfectHistory(1, ZoneId.of("UTC-03:00"));
    perfectHistory.add(10);
    String regex = ".*\\d-03:00\\|10ns";
    assertTrue(perfectHistory.toString().matches(regex));
  }
}
