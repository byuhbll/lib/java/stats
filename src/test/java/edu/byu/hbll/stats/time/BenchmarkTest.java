package edu.byu.hbll.stats.time;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.byu.hbll.stats.time.Benchmark.Data;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BenchmarkTest {

  Benchmark benchmark;

  /**
   * @throws java.lang.Exception
   */
  @BeforeEach
  public void setUp() throws Exception {
    benchmark = new Benchmark(0.1);

    for (int i = 0; i < 4; i++) {
      benchmark.add(i * 1000);
      Thread.sleep(10);
    }
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Benchmark#Benchmark()}. */
  @Test
  public void testBenchmark() {
    Benchmark benchmark = new Benchmark();
    assertEquals(Benchmark.DEFAULT_SMOOTHING_FACTOR, benchmark.getSmoothingFactor(), 0.001);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Benchmark#Benchmark(float)}. */
  @Test
  public void testBenchmarkFloat() {
    Benchmark benchmark = new Benchmark(0.1234f);
    assertEquals(0.1234f, benchmark.getSmoothingFactor(), 0.001);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Benchmark#add(long, long)}. */
  @Test
  public void testAdd() {
    benchmark.add((long) 1e9);
    assertEquals(5, benchmark.snapshot().getCount());
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Benchmark#copy()}. */
  @Test
  public void testCopy() {
    Benchmark copy = benchmark.copy();
    assertEquals(benchmark.getSmoothingFactor(), copy.getSmoothingFactor(), 0.001);
    assertNotEquals(benchmark.snapshot().getCount(), copy.snapshot().getCount());
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Benchmark#toString()}. */
  @Test
  public void testToString() throws Exception {
    Thread.sleep(1100);
    assertEquals("4|885ns|4ps", benchmark.toString());
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Benchmark#getSmoothingFactor()}. */
  @Test
  public void testGetSmoothingFactor() {
    assertEquals(0.1, benchmark.getSmoothingFactor(), 0.001);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Benchmark#getCount()}. */
  @Test
  public void testSnapshot() {
    Data data = benchmark.snapshot();
    assertEquals(4, data.getCount());
    assertEquals(0, data.getMin());
    assertEquals(3000, data.getMax());
    assertEquals(1500, data.getAverage(), .001);
    assertEquals(885, data.getAverageRecent(), .001);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Benchmark#getThroughput()}. */
  @Test
  public void testSnapshotThroughput() throws Exception {
    assertEquals(100, benchmark.snapshot().getThroughput(), 80);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Benchmark#getThroughputRecent()}. */
  @Test
  public void testSnapshotThroughputRecent() throws Exception {
    assertEquals(0, benchmark.snapshot().getThroughputRecent(), 0.001);
    Thread.sleep(1100);
    assertEquals(4, benchmark.snapshot().getThroughputRecent(), 0.001);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Benchmark#getThroughput()}. */
  @Test
  public void testThroughputOutOfOrder() throws Exception {
    benchmark = new Benchmark();
    benchmark.add(1000000);
    benchmark.add(2000000);
    benchmark.add(4000000);
    benchmark.add(8000000);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Benchmark#Benchmark(double, boolean)}. */
  @Test
  public void testBenchmarkDoubleBoolean() throws Exception {
    Benchmark benchmark = new Benchmark(0.1234f, true);
    assertEquals(0.1234f, benchmark.getSmoothingFactor(), 0.001);
    assertTrue(benchmark.isDurationsOnly());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.stats.time.Benchmark#toString(java.util.concurrent.TimeUnit)}.
   */
  @Test
  public void testToStringTimeUnit() throws Exception {
    Benchmark benchmark = new Benchmark(0.1, true);
    benchmark.add(1000);
    assertEquals("1us", benchmark.toString());
    assertEquals("1000ns", benchmark.toString(TimeUnit.NANOSECONDS));
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Benchmark#isDurationsOnly()}. */
  @Test
  public void testIsDurationsOnly() throws Exception {
    assertFalse(benchmark.isDurationsOnly());
  }
}
