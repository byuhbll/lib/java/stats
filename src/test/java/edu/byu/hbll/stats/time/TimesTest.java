package edu.byu.hbll.stats.time;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.byu.hbll.stats.time.Times.Marker;
import java.time.temporal.ChronoUnit;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TimesTest {

  Times<Benchmark> t;

  /**
   * @throws java.lang.Exception
   */
  @BeforeEach
  public void setUp() throws Exception {
    t = Times.benchmark();
    Marker mb = t.marker();
    mb.mark(1);
    mb.mark(2);
    mb.mark(3);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Times#benchmark()}. */
  @Test
  public void testBenchmark() {
    Times<Benchmark> t = Times.benchmark();
    t.marker().mark(1);
    assertTrue(t.getStatistic() instanceof Benchmark);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Times#history()}. */
  @Test
  public void testHistory() {
    Times<History> t = Times.history();
    t.marker().mark(1);
    assertTrue(t.getStatistic() instanceof History);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Times#perfectHistory()}. */
  @Test
  public void testPerfectHistory() {
    Times<PerfectHistory> t = Times.perfectHistory();
    t.marker().mark(1);
    assertTrue(t.getStatistic() instanceof PerfectHistory);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Times#marker()}. */
  @Test
  public void testMarker() {
    Marker m = Times.benchmark().marker();
    assertNotNull(m);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Times.Marker#toString()}. */
  @Test
  public void testToStringBenchmark() throws Exception {
    Times<Benchmark> t = Times.benchmark();
    Marker m = t.marker();
    m.mark(1);
    m.mark(2);
    m.mark(3);
    assertEquals(1, t.toString().split("\n").length);
    assertEquals(1, m.toString().split("\n").length);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Times.Marker#toString()}. */
  @Test
  public void testToStringHistory() throws Exception {
    Times<History> t = new Times<>(new History(ChronoUnit.MILLIS, 100));
    Marker m = t.marker();
    m.mark(1);
    Thread.sleep(2);
    m.mark(2);
    Thread.sleep(2);
    m.mark(3);
    assertTrue(t.toString().split("\n").length >= 3);
    assertTrue(m.toString().split("\n").length >= 3);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Times.Marker#toString()}. */
  @Test
  public void testToStringPerfectHistory() throws Exception {
    Times<PerfectHistory> t = Times.perfectHistory();
    Marker m = t.marker();
    m.mark(1);
    m.mark(2);
    m.mark(3);
    assertEquals(3, t.toString().split("\n").length);
    assertEquals(3, m.toString().split("\n").length);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Times#getLabels()}. */
  @Test
  public void testGetLabels() throws Exception {
    List<Object> labels = t.getLabels();
    assertEquals(1, labels.get(0));
    assertEquals(2, labels.get(1));
    assertEquals(3, labels.get(2));
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Times#getStatistic()}. */
  @Test
  public void testGetStatistic() throws Exception {
    assertEquals(t.getStatistic(1), t.getStatistic());
    assertNull(Times.benchmark().getStatistic());
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Times#getStatistic(java.lang.Object)}. */
  @Test
  public void testGetStatisticObject() throws Exception {
    assertNotNull(t.getStatistic(1));
    assertNotNull(t.getStatistic(2));
    assertNotNull(t.getStatistic(3));
    assertNull(t.getStatistic(4));
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Times#getStatistics()}. */
  @Test
  public void testGetStatistics() throws Exception {
    List<Benchmark> stats = t.getStatistics();
    assertEquals(t.getStatistic(1), stats.get(0));
    assertEquals(t.getStatistic(2), stats.get(1));
    assertEquals(t.getStatistic(3), stats.get(2));
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Times#mark()}. */
  @Test
  public void testMark() throws Exception {
    Times<Benchmark> t = Times.benchmark();
    t.mark();
    t.mark();
    t.mark();
    Benchmark b = t.getStatistic(1);
    assertNotNull(b);
    assertEquals(3, b.snapshot().getCount());
    assertEquals(0, b.snapshot().getAverage(), .001);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Times#mark(java.lang.Object)}. */
  @Test
  public void testMarkObject() throws Exception {
    Times<Benchmark> t = Times.benchmark();
    t.mark(1);
    t.mark(2);
    t.mark(3);
    Benchmark b = t.getStatistic(2);
    assertNotNull(b);
    assertEquals(1, b.snapshot().getCount());
    assertEquals(0, b.snapshot().getAverage(), .001);
  }

  /** Test method for {@link edu.byu.hbll.stats.time.Times#single()}. */
  @Test
  public void testSingle() throws Exception {
    Marker m = Times.single();

    for (int i = 0; i < 4; i++) {
      m.mark(i);
      Thread.sleep(1);
    }

    String regex = "0-.* 1-.* 2-.* 3-.*";
    assertTrue(m.toString().matches(regex));
  }
}
