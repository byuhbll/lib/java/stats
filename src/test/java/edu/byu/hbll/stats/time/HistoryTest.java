package edu.byu.hbll.stats.time;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.byu.hbll.stats.time.History.Data;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalUnit;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class HistoryTest {

  History history;
  // pick a large enough unit that the tests are very likely to pass
  TemporalUnit timeUnit = new VarUnit(Duration.ofMillis(60));
  ZonedDateTime startTime;

  /**
   * @throws java.lang.Exception
   */
  @BeforeEach
  public void setUp() throws Exception {
    history = new History(timeUnit, 4);
    startTime = ZonedDateTime.now();

    for (int i = 0; i < 5; i++) {
      history.add(i);
      Thread.sleep(timeUnit.getDuration().toMillis());
    }
  }

  /** Test method for {@link edu.byu.hbll.stats.time.History#History()}. */
  @Test
  public void testHistory() {
    History history = new History();
    assertEquals(History.DEFAULT_RESOLUTION, history.getResolution());
    assertEquals(History.DEFAULT_MAX, history.getMax());
  }

  /**
   * Test method for {@link edu.byu.hbll.stats.time.History#History(java.time.temporal.ChronoUnit,
   * int)}.
   */
  @Test
  public void testHistoryChronoUnitInt() {
    History history = new History(ChronoUnit.MICROS, 17);
    assertEquals(ChronoUnit.MICROS, history.getResolution());
    assertEquals(17, history.getMax());
  }

  /** Test method for {@link edu.byu.hbll.stats.time.History#add(long, long)}. */
  @Test
  public void testAdd() {
    history.add(0);
    assertEquals(4, history.snapshot().size());
  }

  /** Test method for {@link edu.byu.hbll.stats.time.History#copy()}. */
  @Test
  public void testCopy() {
    History copy = history.copy();
    assertEquals(history.getResolution(), copy.getResolution());
    assertEquals(history.getMax(), copy.getMax());
  }

  /** Test method for {@link edu.byu.hbll.stats.time.History#toString()}. */
  @Test
  public void testToString() {
    String matchString = ".*1\\|4ns\n.*1\\|3ns\n.*1\\|2ns\n.*1\\|1ns.*";
    String historyString = history.toString();
    assertTrue(historyString.matches(matchString));
  }

  /** Test method for {@link edu.byu.hbll.stats.time.History#getResolution()}. */
  @Test
  public void testGetResolution() {
    assertEquals(timeUnit, history.getResolution());
  }

  /** Test method for {@link edu.byu.hbll.stats.time.History#getMax()}. */
  @Test
  public void testGetMax() {
    assertEquals(4, history.getMax());
  }

  /** Test method for {@link edu.byu.hbll.stats.time.History#snapshot()}. */
  @Test
  public void testSnapshot() throws Exception {
    List<Data> data = history.snapshot();
    assertEquals(4, data.size());

    for (int i = 0; i < 4; i++) {
      assertEquals(1, data.get(i).getCount());
      assertEquals(Duration.ofNanos(i + 1), data.get(i).getDuration());
      assertEquals(startTime.plus(i + 1, timeUnit).truncatedTo(timeUnit), data.get(i).getTime());
    }
  }

  private static class VarUnit implements TemporalUnit {

    private Duration duration;

    private VarUnit(Duration duration) {
      this.duration = duration;
    }

    @Override
    public Duration getDuration() {
      return duration;
    }

    @Override
    public boolean isDurationEstimated() {
      return false;
    }

    @Override
    public boolean isDateBased() {
      return false;
    }

    @Override
    public boolean isTimeBased() {
      return true;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <R extends Temporal> R addTo(R temporal, long amount) {
      return (R) temporal.plus(duration.multipliedBy(amount));
    }

    @Override
    public long between(Temporal temporal1Inclusive, Temporal temporal2Exclusive) {
      return temporal1Inclusive.until(temporal2Exclusive, this);
    }
  }

  /**
   * Test method for {@link edu.byu.hbll.stats.time.History#History(java.time.temporal.TemporalUnit,
   * int, java.time.ZoneId)}.
   */
  @Test
  public void testHistoryTemporalUnitIntZoneId() throws Exception {
    History history = new History(timeUnit, 1, ZoneId.of("UTC-03:00"));
    history.add(10);
    String regex = ".*\\d-03:00\\|1\\|10ns";
    assertTrue(history.toString().matches(regex));
  }
}
