package edu.byu.hbll.stats.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Test;

public class FormatUtilsTest {

  /**
   * Test method for {@link
   * edu.byu.hbll.stats.time.FormatUtils#abbreviation(java.util.concurrent.TimeUnit)}.
   */
  @Test
  public void testAbbreviation() {
    assertEquals("ns", FormatUtils.abbreviation(TimeUnit.NANOSECONDS));
    assertEquals("us", FormatUtils.abbreviation(TimeUnit.MICROSECONDS));
    assertEquals("ms", FormatUtils.abbreviation(TimeUnit.MILLISECONDS));
    assertEquals("s", FormatUtils.abbreviation(TimeUnit.SECONDS));
    assertEquals("m", FormatUtils.abbreviation(TimeUnit.MINUTES));
    assertEquals("h", FormatUtils.abbreviation(TimeUnit.HOURS));
    assertEquals("d", FormatUtils.abbreviation(TimeUnit.DAYS));
  }

  /** Test method for {@link edu.byu.hbll.stats.time.FormatUtils#format(double, int)}. */
  @Test
  public void testFormat() {
    assertEquals("1.23", FormatUtils.format(1.234, 3));
    assertEquals("1.2340", FormatUtils.format(1.234, 5));
  }

  /** Test method for {@link edu.byu.hbll.stats.time.FormatUtils#formatDuration(double)}. */
  @Test
  public void testFormatDurationDouble() {
    assertEquals("1.23us", FormatUtils.formatDuration(1234));
  }

  /** Test method for {@link edu.byu.hbll.stats.time.FormatUtils#formatDuration(double, int)}. */
  @Test
  public void testFormatDurationDoubleInt() {
    assertEquals("1.234us", FormatUtils.formatDuration(1234, 4));
  }

  /**
   * Test method for {@link edu.byu.hbll.stats.time.FormatUtils#formatDuration(double, int,
   * java.util.concurrent.TimeUnit)}.
   */
  @Test
  public void testFormatDurationDoubleIntTimeUnit() {
    assertEquals("0.00123ms", FormatUtils.formatDuration(1234, 3, TimeUnit.MILLISECONDS));
  }

  /** Test method for {@link edu.byu.hbll.stats.time.FormatUtils#formatThroughput(double)}. */
  @Test
  public void testFormatThroughputDouble() {
    assertEquals("12.3ps", FormatUtils.formatThroughput(12.34));
  }

  /** Test method for {@link edu.byu.hbll.stats.time.FormatUtils#formatThroughput(double, int)}. */
  @Test
  public void testFormatThroughputDoubleInt() {
    assertEquals("12.34ps", FormatUtils.formatThroughput(12.34, 4));
  }

  /**
   * Test method for {@link edu.byu.hbll.stats.time.FormatUtils#formatThroughput(double, int,
   * java.util.concurrent.TimeUnit)}.
   */
  @Test
  public void testFormatThroughputDoubleIntTimeUnit() {
    assertEquals("740pm", FormatUtils.formatThroughput(12.34, 3, TimeUnit.MINUTES));
  }

  /** Test method for {@link edu.byu.hbll.stats.time.FormatUtils#unitDuration(double)}. */
  @Test
  public void testUnitDuration() {
    assertEquals(TimeUnit.NANOSECONDS, FormatUtils.unitDuration(1));
    assertEquals(TimeUnit.MICROSECONDS, FormatUtils.unitDuration(TimeUnit.MICROSECONDS.toNanos(1)));
    assertEquals(TimeUnit.MILLISECONDS, FormatUtils.unitDuration(TimeUnit.MILLISECONDS.toNanos(1)));
    assertEquals(TimeUnit.SECONDS, FormatUtils.unitDuration(TimeUnit.SECONDS.toNanos(1)));
    assertEquals(TimeUnit.MINUTES, FormatUtils.unitDuration(TimeUnit.MINUTES.toNanos(1)));
    assertEquals(TimeUnit.HOURS, FormatUtils.unitDuration(TimeUnit.HOURS.toNanos(1)));
    assertEquals(TimeUnit.DAYS, FormatUtils.unitDuration(TimeUnit.DAYS.toNanos(1)));
  }

  /** Test method for {@link edu.byu.hbll.stats.time.FormatUtils#unitThroughput(double)}. */
  @Test
  public void testUnitThroughput() {
    assertEquals(TimeUnit.SECONDS, FormatUtils.unitThroughput(1));
    assertEquals(TimeUnit.MINUTES, FormatUtils.unitThroughput(1d / 60));
    assertEquals(TimeUnit.HOURS, FormatUtils.unitThroughput(1d / 3600));
    assertEquals(TimeUnit.DAYS, FormatUtils.unitThroughput(1d / 86400));
  }
}
